<?php
use Behat\Behat;
use Behat\Gherkin\Lexer;
use Behat\Gherkin\Node\BackgroundNode;
use Behat\Gherkin\Node\FeatureNode;
use Behat\Gherkin\Node\OutlineNode;
use Behat\Gherkin\Node\ScenarioInterface;
use Behat\Gherkin\Node\ScenarioNode;
use Behat\Gherkin\Node\StepContainerInterface;
use Behat\Gherkin\Parser;
use value_fetcher\ValueFetcher;
use Symfony\Component\Yaml\Yaml;

class MappingGenerator {
    use Base;

    private $config;
    private $separator = ';';
    /**
     * @var \gsheets_api\ValueFetcher
     */
    private $values = null;

    private $parser = null;
    private $featuresList;
    private $found = 0;
    private $total = 0;


    /**
     * GherkinToMarkdown constructor.
     */
    public function __construct() {
        $this->config = Yaml::parse(file_get_contents(
            dirname(__FILE__) . '/../config.yml'
        ));

        if (!isset($this->config)) {
            throw new Exception("\n\nConfig file not found!\n\n");
        }

        $this->values = new ValueFetcher();
        $this->values->updateData();
    }

    /**
     * Runner
     */
    public function execute() {
        $keywords = $this->initializeKeyWords();
        $lexer = new Lexer($keywords);
        $this->parser = new Parser($lexer);

        $this->featuresList = $this->getFeaturesList();
        $this->readTests($this->featuresList);
        $this->values->updateGSheet();
    }

    /**
     * Prints and entire test suite
     *
     * @param $featuresArray
     */
    private function readTests($featuresArray) {
        foreach ($featuresArray as $feature) {
            echo "\n\n\e[1;29mParsing $feature\e[0m";
            $parsedFeature = $this->parser->parse(file_get_contents($feature));
            $this->readFeature($parsedFeature);
        }
    }

    /**
     * Prints a single Feature
     *
     * @param FeatureNode $feature
     */
    private function readFeature(FeatureNode $feature) {
        if ($feature->getBackground() != null) {
            $this->readScenario($feature->getBackground());
        }
        foreach ($feature->getScenarios() as $scenario) {
            $this->readScenario($scenario);
        }
    }

    /**
     * Prints the scenario
     *
     * @param StepContainerInterface $scenario
     */
    private function readScenario(StepContainerInterface $scenario) {
        $steps = $scenario->getSteps();
        foreach ($steps as $step) {
            if ($step->hasArguments()) {
                $this->convertTable($step->getArguments(), true);
            }
            $step = $step->getText();
            preg_match_all("/[A-Z 0-9_]+\/[A-Z 0-9_]+/", $step, $matches);
            foreach ($matches[0] as $input){
                $this->convertToConstant($input);
            }
        }
        if ($scenario instanceof OutlineNode) {
            $this->convertTable($scenario->getExampleTable()->getTable());
        }
    }

    private function convertTable(array $table, $arguments = false) {
        foreach ($table as $line){
            foreach ($line as $cell) {
                if ($arguments) {
                    foreach ($cell as $value)
                        $this->convertToConstant($value);
                } else {
                    $this->convertToConstant($cell);
                }
            }
        }
    }

    private function addConstant($name, $map) {
        $this->values->addConstantValue('AUTO', $map, $name);
    }

    /**
     * Fetches the list af all the .features files, using features_regex_selector from config.yml
     *
     * @return array
     */
    private function getFeaturesList() {
        $pattern = $this->config['config']['features_regex_selector'];
        $return_array = array();

        foreach (glob($pattern) as $filename) {
            $return_array[] = $filename;
        }

        if (sizeof($return_array) == 0) {
            throw new Exception("\n\nDid not find any feature in $pattern!\n\n");
        }

        return $return_array;
    }

    private function convertToConstant($input) {
        if ($this->isConstant($input)) {
            $set = explode("/", $input, 2);
            $this->addConstant($set[1], $set[0]);
        }
    }

    private function isConstant($value) {
        return preg_match("/[A-Z 0-9_]+\/[A-Z 0-9_]+/", $value, $matches);
    }
}
