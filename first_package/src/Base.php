<?php


trait Base {

    /**
     * Returns all the input of a given step (".*")
     *
     * @param $step
     * @return mixed
     */
    public static function getInputs($step)
    {
        preg_match('/".*"/', $step, $outputArray);
        if (isset($outputArray))
            for ($i = 0; $i < sizeof($outputArray); $i++)
                $outputArray[$i] = str_replace('"', "", $outputArray[$i]);
        return $outputArray;
    }

    /**
     * Generates the keys array needed for Gherkin parsing
     *
     * @return \Behat\Gherkin\Keywords\ArrayKeywords
     */
    public static function initializeKeyWords() {
        return new Behat\Gherkin\Keywords\ArrayKeywords(array(
            'en' => array(
                'feature'          => 'Feature',
                'background'       => 'Background',
                'scenario'         => 'Scenario',
                'scenario_outline' => 'Scenario Outline|Scenario Template',
                'examples'         => 'Examples|Scenarios',
                'given'            => 'Given',
                'when'             => 'When',
                'then'             => 'Then',
                'and'              => 'And',
                'but'              => 'But'
            )));
    }
}