<?php
namespace value_fetcher\interfaces;

interface ValueFetcherInterface {
    /**
     * Instantiates and updates locally the values from the google sheet
     */
    function updateData();

    /**
     * Instantiates and updates the values from the google sheet, then performs a search for the Key value
     *
     * @param $page
     * @param $key
     * @param $column
     * @param $section
     * @return string|int|null
     */
    function getUpdatedValue($page, $key, $column, $section);

    /**
     * Performs a search for the Key value
     *
     * @param $page
     * @param $key
     * @param $column
     * @param $section
     * @return string|int|null
     */
    function getValue($page, $key, $column, $section);

}