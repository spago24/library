<?php
namespace value_fetcher\interfaces;

interface GoogleConstants {
    const SPREADSHEET_ID = '1k7ftmORXtcnpf9enPwEFjuSvTCV0HHjY70uAPtjbqzo';
    const PAGES = array(
        'AUTO'
    );

    const CREDENTIALS_PATH = __DIR__ . '/../../../../../features/bootstrap/gsheets_api/credentials.json';
    const TOKEN_PATH = __DIR__ . '/../../../../../features/bootstrap/gsheets_api/token.json';

    const DEFAULT_VALUE_COLUMN = 'DEFAULT';
    const DEFAULT_SECTION_COLUMN = 'MAPPING';
}