<?php
namespace value_fetcher;
use Exception;
use value_fetcher\interfaces\GoogleConstants;
use value_fetcher\interfaces\ValueInterface;
use mysql_xdevapi\Table;

class Values implements ValueInterface, GoogleConstants {
    private $values;
    private $header;

    public function __construct() {
        $this->values = array();
    }

    //matrice->to hashmap
    function addTable($name, $table) {
        print('var_dump($table); ');
        var_dump($table);
        $row = 1;
        $this->header = $table[0]; // prima riga
        while (isset($table[$row])) {
            $itemsRow = array();
            $section = $table[$row][0]; // sezione
            $key = $table[$row][1]; // chiave
            for ($column = 2; $column < (count($this->header)); $column++) {
                if(isset($table[$row][$column])) {
                    $cell = $table[$row][$column];
                } else {
                    $cell = 'null';
                }
                $itemsRow[$this->header[$column]] = $cell;
            }
            $this->values[$name][$section][$key] = $itemsRow;
            $row++;
        }
        print('var_dump($values): ');
        var_dump($this->values);
    }

    public function toTable(){
        $sheetNames = array_keys($this->values); //nomi del foglio
        print('$sheetName: '.$sheetNames[0]);

        $newTables = array(); //array che contiene pagine di diversi fogli
        foreach ($sheetNames as $sheet){
            $newTable = []; //una pagina
            $newTable[0]=$this->header;
            //print_r($newTable);
            $row = Array ();
                $section = array_keys( $this->values[$sheet])[0]; //prima cella
                array_push($row,$section);
                $key = array_keys( $this->values[$sheet][$section])[0]; //seconda cella
                array_push($row,$key);

                for($i=0; $i<count($this->values[$sheet][$section][$key]); $i++) {
                    $cell = $this->values[$sheet][$section][$key][array_keys($this->values[$sheet][$section][$key])[$i]];

                    array_push($row,$cell);
                    /*
                    print('$cell: '.$cell);
                    print(' print_r($row): ');
                    print_r($row);
                    */
                }
                print (' $row [0]: '.  $row [0]."\n");
                print (' $row [1]: '.  $row [1]."\n");
                print (' $row [2]: '.$row[2]."\n");
                print (' $row [3]: '.$row[3]."\n");
                print (' $row [4]: '.$row[4]."\n");
                print (' $row [5]: '.$row[5]."\n");
                print (' $row [6]: '.$row[6]."\n");

                print(' print_r($row): ');
                print_r($row);

                $newTable[1]=$row;
                print_r($newTable);
        }
        $newTables[0]=$newTable;
        print_r($newTables);
    }

    function getValue($page, $key, $column, $section = self::DEFAULT_SECTION_COLUMN) {
        if(isset($this->values[$page][$section][$key][$column])) {
            if ($this->values[$page][$section][$key][$column] === '') {
                return null;
            }
            return $this->values[$page][$section][$key][$column];
        }
        return null;
    }

    function addValue($page, $section, $key) {
        $this->values[$page][$section][$key][self::DEFAULT_VALUE_COLUMN] = "TODO: set this value";
    }

    function resetTables() {
        unset($this->values);
        $this->values = array();
    }
}