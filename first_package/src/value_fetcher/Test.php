<?php

use value_fetcher\ValueFetcher;

require __DIR__ . '/../../vendor/autoload.php';

$values = new ValueFetcher();
$values->updateData();
$values->addConstantValue("AUTO", 'SECTION_TEST', 'KEY_TEST');
$values->getValue("AUTO", 'KEY_TEST', "DEFAULT", 'SECTION_TEST');
$values->addConstantValue("AUTO", 'SECTION_TEST2', 'KEY_TEST2');
$values->getValue("AUTO", 'KEY_TEST2', "DEFAULT", 'SECTION_TEST2');
$values->addConstantValue("AUTO", 'SECTION_TEST', 'KEY_TEST');
$values->getValue("AUTO", 'KEY_TEST', "DEFAULT", 'SECTION_TEST');
//$runner->execute();