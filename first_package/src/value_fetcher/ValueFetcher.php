<?php
namespace value_fetcher;
use Exception;
use Google_Service_Sheets;
use Google_Client;
use Google_Service_Sheets_ValueRange;
use value_fetcher\interfaces\GoogleConstants;
use value_fetcher\interfaces\ValueFetcherInterface;


class ValueFetcher implements ValueFetcherInterface, GoogleConstants {
    /**
     * @var Values
     */
    private $data = null;

    private $spreadsheetId = self::SPREADSHEET_ID;

    /**
     * @inheritDoc
     */
    function updateData() {
        echo "Updating Google Sheets data\n";
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);

        $ranges = self::PAGES;
        $params = array(
            'majorDimension' => 'ROWS'
        );
        if (!isset($this->data)) {
            $this->data = new Values();
        }
        foreach ($ranges as $pageName) {
            $response = $service->spreadsheets_values->get($this->spreadsheetId, $pageName, $params);
            $table = $response->getValues();
            $this->data->addTable($pageName, $table);
        }
    }

    /**
     * @inheritDoc
     */
    function updateGSheet() {
    /*
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);

        $values = [
                //valori da scrivere sul foglio
            $this->data->toTable()
        ];
        $body = new Google_Service_Sheets_ValueRange([
            'values' => $values
        ]);
        $params = [
            'valueInputOption' => 'RAW'
        ];
        $range = "$page!A3";
        $result = $service->spreadsheets_values->update($this->spreadsheetId, $range, $body, $params);
        //$result = $service->spreadsheets_values->update($spreadsheetId, null,
        //    $body, $params);
        printf("%d cells updated.", $result->getUpdatedCells());
    */
        $this->data->toTable();
    }

    /**
     * @inheritDoc
     */
    function getUpdatedValue($page, $key, $column = self::DEFAULT_VALUE_COLUMN, $section = self::DEFAULT_SECTION_COLUMN) {
        $this->updateData();
        return $this->getValue($page, $key, $column, $section);
    }

    /**
     * @inheritDoc
     */
    function getValue($page, $key, $column = self::DEFAULT_VALUE_COLUMN, $section = self::DEFAULT_SECTION_COLUMN) {
        $value = $this->data->getValue($page, $key, $column, $section);
        if (!isset($value) && $section!=self::DEFAULT_SECTION_COLUMN) {
            echo "Value for $page, $section, $key, $column not found in google sheet, retry with DEFAULT for $section\n";
            return $this->getValue($page, $key, self::DEFAULT_VALUE_COLUMN, $section);
        }
        if (!isset($value) && $column!=self::DEFAULT_VALUE_COLUMN) {
            echo "Value for $page, $section, $key, $column not found in google sheet, retry with DEFAULT and MAPPING\n";
            return $this->getValue($page, $key);
        }
        elseif (!isset($value)) {
            echo "Value for $page, $section, $key, $column not found in google sheet, retry using YAML Data\n";
            return null;
        } else {
            echo "Value for $page, $section, $key, $column found, equals: $value\n";
            return $value;
        }
    }

    /**
     * @inheritDoc
     */
    function addConstantValue($page, $section, $key) {
        $value = $this->data->getValue($page, $key, 'DEFAULT', $section);
        if (!isset($value)) {
            echo "\e[1;32m\nFound and added new constant value $key in $section, $page\e[0m";
            $this->data->addValue($page, $section ,$key);
            //$this->updateGSheet($page, $range);
        }
    }

    /**
     * Returns an authorized API client.
     * @return Google_Client the authorized client object
     */
    private function getClient()
    {
        if (php_sapi_name() != 'cli') {
            throw new Exception('This application must be run on the command line.');
        }

        $client = new Google_Client();
        $client->setApplicationName('Google Sheets API PHP SQA');
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
        $client->setAuthConfig(self::CREDENTIALS_PATH);
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = self::TOKEN_PATH;
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

}