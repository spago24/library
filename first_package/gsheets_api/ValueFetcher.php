<?php
namespace gsheets_api {
    use Exception;
    use Google_Service_Sheets;
    use Google_Client;
    use Google_Service_Sheets_ValueRange;
    use gsheets_api\interfaces\GoogleConstants;
    use gsheets_api\interfaces\ValueFetcherInterface;


    class ValueFetcher implements ValueFetcherInterface, GoogleConstants {
        /**
         * @var Values
         */
        private $data = null;

        /**
         * @inheritDoc
         */
        function updateData() {
            echo "Updating Google Sheets data\n";
            if (!isset($this->data)) {
                $this->data = new Values();
            }
            $this->data->resetTables();
            $client = $this->getClient();
            $service = new Google_Service_Sheets($client);

            $spreadsheetId = self::SPREADSHEET_ID;
            $ranges = self::PAGES;
            $params = array(
                'majorDimension' => 'ROWS'
            );

            foreach ($ranges as $pageName) {
                $response = $service->spreadsheets_values->get($spreadsheetId, $pageName, $params);
                $table = $response->getValues();
                $this->data->addTable($pageName, $table);
            }
        }

        /**
         * @inheritDoc
         */
        function updateUserPassword($newPassword, $country) {
            echo "Updating user password on Google Sheet\n";
            $client = $this->getClient();
            $service = new Google_Service_Sheets($client);

            $spreadsheetId = self::SPREADSHEET_ID;
            // get country index from google constants and append it to range
            $countryIndex = self::COUNTRYINDEXES[$country];
            $range = "USERS!{$countryIndex}3";

            $updateBody = new Google_Service_Sheets_ValueRange([
                'range' => $range,
                'majorDimension' => 'ROWS',
                'values' => [
                    [$newPassword]
                ]
            ]);
            $service->spreadsheets_values->update(
                $spreadsheetId,
                $range,
                $updateBody,
                ['valueInputOption' => "RAW"]
            );
        }

        /**
         * @inheritDoc
         */
        function getUpdatedValue($page, $key, $column = self::DEFAULT_VALUE_COLUMN, $section = self::DEFAULT_SECTION_COLUMN) {
            $this->updateData();
            return $this->getValue($page, $key, $column, $section);
        }

        /**
         * @inheritDoc
         */
        function getValue($page, $key, $column = self::DEFAULT_VALUE_COLUMN, $section = self::DEFAULT_SECTION_COLUMN) {
            $value = $this->data->getValue($page, $key, $column, $section);
            if (!isset($value) && $section!=self::DEFAULT_SECTION_COLUMN) {
                echo "Value for $page, $section, $key, $column not found in google sheet, retry with DEFAULT for $section\n";
                return $this->getValue($page, $key, self::DEFAULT_VALUE_COLUMN, $section);
            }
            if (!isset($value) && $column!=self::DEFAULT_VALUE_COLUMN) {
                echo "Value for $page, $section, $key, $column not found in google sheet, retry with DEFAULT and MAPPING as Section\n";
                return $this->getValue($page, $key);
            }
            elseif (!isset($value)) {
                echo "Value for $page, $section, $key, $column not found in google sheet, retry using YAML Data\n";
                return null;
            } else {
                echo "Value for $page, $section, $key, $column found, equals: $value\n";
                return $value;
            }
        }

        /**
         * Returns an authorized API client.
         * @return Google_Client the authorized client object
         */
        private function getClient()
        {
            if (php_sapi_name() != 'cli') {
                throw new Exception('This application must be run on the command line.');
            }

            $client = new Google_Client();
            $client->setApplicationName('Google Sheets API PHP SQA');
            $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
            $client->setAuthConfig(self::CREDENTIALS_PATH);
            $client->setAccessType('offline');
            $client->setPrompt('select_account consent');

            // Load previously authorized token from a file, if it exists.
            // The file token.json stores the user's access and refresh tokens, and is
            // created automatically when the authorization flow completes for the first
            // time.
            $tokenPath = self::TOKEN_PATH;
            if (file_exists($tokenPath)) {
                $accessToken = json_decode(file_get_contents($tokenPath), true);
                $client->setAccessToken($accessToken);
            }

            // If there is no previous token or it's expired.
            if ($client->isAccessTokenExpired()) {
                // Refresh the token if possible, else fetch a new one.
                if ($client->getRefreshToken()) {
                    $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                } else {
                    // Request authorization from the user.
                    $authUrl = $client->createAuthUrl();
                    printf("Open the following link in your browser:\n%s\n", $authUrl);
                    print 'Enter verification code: ';
                    $authCode = trim(fgets(STDIN));

                    // Exchange authorization code for an access token.
                    $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                    $client->setAccessToken($accessToken);

                    // Check to see if there was an error.
                    if (array_key_exists('error', $accessToken)) {
                        throw new Exception(join(', ', $accessToken));
                    }
                }
                // Save the token to a file.
                if (!file_exists(dirname($tokenPath))) {
                    mkdir(dirname($tokenPath), 0700, true);
                }
                file_put_contents($tokenPath, json_encode($client->getAccessToken()));
            }
            return $client;
        }

    }
}