<?php
namespace gsheets_api\interfaces;

interface ValueInterface {
    /**
     * Adds a page to the values list
     *
     * @param $name
     * @param array $values
     * @return mixed
     */
    function addTable($name, $values);

    /**
     * @param $page
     * @param $key
     * @param $column
     * @param $section
     * @return mixed
     */
    function getValue($page, $key, $column, $section);
    function resetTables();

}