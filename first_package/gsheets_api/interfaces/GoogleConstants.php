<?php
namespace gsheets_api\interfaces;

interface GoogleConstants {
    const SPREADSHEET_ID = '1k7ftmORXtcnpf9enPwEFjuSvTCV0HHjY70uAPtjbqzo';
    const PAGES = array(
        'CONSTANTS',
        'MAPPING',
        'TEST_ENVIRONMENT',
        'USERS',
        'AUTO'
    );

    const DEFAULT_VALUE_COLUMN = 'DEFAULT';
    const DEFAULT_SECTION_COLUMN = 'MAPPING';

    const CREDENTIALS_PATH = __DIR__ . '/../credentials.json';
    const TOKEN_PATH = __DIR__ . '/../token.json';

    const COUNTRYINDEXES = [
        'Italy_SafiloB2BDT_SIT' => 'C',
        'UK_SafiloB2BDT_SIT' => 'D',
        'Czech_SafiloB2BDT_SIT' => 'E',
        'France_SafiloB2BDT_SIT' => 'F',
        'Germany_SafiloB2BDT_SIT' => 'G',
        'Hungary_SafiloB2BDT_SIT' => 'H',
        'Slovakia_SafiloB2BDT_SIT' => 'I',
        'Turkey_SafiloB2BDT_SIT' => 'J',
        'Spain_SafiloB2BDT_SIT' => 'K',
        'Netherlands_SafiloB2BDT_SIT' => 'L',
        'Belgium_SafiloB2BDT_SIT' => 'M',
        'Luxembourg_SafiloB2BDT_SIT' => 'N',
        'Finland_SafiloB2BDT_SIT' => 'O',
        'Norway_SafiloB2BDT_SIT' => 'P',
        'Sweden_SafiloB2BDT_SIT' => 'Q',
        'Denmark_SafiloB2BDT_SIT' => 'R',
        'Estonia_SafiloB2BDT_SIT' => 'S',
        'Latvia_SafiloB2BDT_SIT' => 'T',
        'Lithuania_SafiloB2BDT_SIT' => 'U',
        'Switzerland_SafiloB2BDT_SIT' => 'V',
        'Austria_SafiloB2BDT_SIT' => 'W',
    ];
}