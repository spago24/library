<?php
namespace gsheets_api;
use Exception;
use gsheets_api\interfaces\GoogleConstants;
use gsheets_api\interfaces\ValueInterface;

class Values implements ValueInterface, GoogleConstants {
    private $values;

    public function __construct() {
        $this->values = array();
    }

    function addTable($name, $table) {
        $row = 1;
        $header = $table[0]; // prima riga
        while (isset($table[$row])) {
            $itemsRow = array();
            $section = $table[$row][0]; // sezione
            $key = $table[$row][1]; // chiave
            for ($column = 2; $column < (count($header)); $column++) {
                if(isset($table[$row][$column])) {
                    $cell = $table[$row][$column];
                } else {
                    $cell = null;
                }
                $itemsRow[$header[$column]] = $cell;
            }
            $this->values[$name][$section][$key] = $itemsRow;
            $row++;
        }
    }

    function getValue($page, $key, $column, $section = self::DEFAULT_SECTION_COLUMN) {
        if(isset($this->values[$page][$section][$key][$column])) {
            if ($this->values[$page][$section][$key][$column] === '') {
                return null;
            }
            return $this->values[$page][$section][$key][$column];
        }
        return null;
    }

    function resetTables() {
        unset($this->values);
        $this->values = array();
    }
}